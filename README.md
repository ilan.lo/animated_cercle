# Animated cercle

Demande une taille initiale du cercle, l'affiche puis change la couleur, la taille, ainsi que les caracteres utilisés pour le cercle
Algorithme s'éxécutant dans un terminal Linux, sans cela l'ecran ne s'effacera pas entre chaque etat du cercle.
(Utilisation de la commande system("clear");)
Le code fonctionne en IDE mais le message d'erreur suivant peut s'afficher
" 'clear' n’est pas reconnu en tant que commande interne
ou externe, un programme exécutable ou un fichier de commandes."
