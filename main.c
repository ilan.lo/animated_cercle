#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int create_cercle(int a,char w)
{
    for (int i=0; i<a; i++) {
        for (int j=0; j<a; j++) {
            int x = i-a/2;
            int y = a-1-j-a/2;
            if (x*x+y*y<=a*a/4) printf("%c%c",w,w);
            else printf("  ");
        }
        printf("\n");
    }
    return 0;
}
int change_size(int a){
    if (a>40) a=10;
    else a=a+1;
    return (a);
}

int couleur(int a){
    switch (a%5){
        case 0: printf("\x1b[1;93m");
            break;
        case 1: printf("\x1b[1;37m");
            break;
        case 2:printf("\x1b[1;31m");
            break;

        case 3:printf("\x1b[1;34m");
            break;

        case 4:printf("\x1b[1;36m");
            break;
    }
}


int main(){
    int size;
    int frame=0;
    printf("entrez la taille initiale du cercle");
    scanf("%d",&size);
    while (1){
        system("clear");
        couleur(frame);
        create_cercle(size,'#');
        frame++;
        usleep(500000);
        system("clear");
        couleur(frame);
        size =change_size(size);
        create_cercle(size,'O');
        frame++;
        usleep(500000);
    }
}
